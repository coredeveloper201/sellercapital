const Product = require('../models/product');
const config = require('../config/main');
const async = require("async");
const moment = require("moment");

var scraper = require('../library/product-scraper');
// const AmazonScraper = require('amazon-scraper')

const mws_key = config.MWS_KEY || '';
const mws_access = config.MWS_SECRET || '';
var amazonMws = require('amazon-mws')('AKIAIEGT53RIXYQUCTPQ','VdToubCLaeVs+ngo3g7aIGCUzlqsisfCVWnKCga6');


exports.getProducts = function (req, res, next) {
    const SellerId = 'A1LWZ980X488GK';
    const MWSAuthToken = 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8';
    const MarketplaceId = 'ATVPDKIKX0DER';
    amazonMws.products.search({
        'Version': '2011-10-01',
        'Action': 'ListMatchingProducts',
        'SellerId': SellerId,
        'MWSAuthToken': MWSAuthToken,
        'MarketplaceId': MarketplaceId,
        'Query': 'k'
    }, function (error, response) {
        if (error) {
            console.log('error ', error);
            return;
        }
        if(response && Object.keys(response).length){
            // let orders = formatProductData(response);
            return res.status(200).json(response);
        }

    });
}

// scrap single product info
// product scrapper
exports.singleProduct = function (req, res, next){
    // scraper.init('http://www.amazon.com/gp/product/B07GJJJGCL/', function(data){
    scraper.init('http://www.amazon.com/gp/product/B01N1UX8RW/', function(data){
        return res.status(200).json(data);
    });
}


 
//  Scrapping with Amazon scraping
// exports.singleProduct = function (req, res, next){
//     const amazon_scraper = AmazonScraper({
//                             "product_url": "/Fujifilm-Systemkamera-Fujinon-Objektiv-Megapixel/dp/B00XW693XE/ref=sr_1_3?ie=UTF8&qid=1476031611&sr=8-3&keywords=fuji+xt10",
//                             "lang": ["de", "it", "co.uk", "fr"]
//                         });
 
//     amazon_scraper.scraper.then(function(data) {
//         // return res.status(200).json(data);
//         return amazon_scraper.printTable(data)
//     });

// }


//  For test purpose
exports.deleteProducts = function (req, res, next) {
    Product.remove({}, (err, deletedProducts) => {
        if (err) {
            console.log(err);
        }
        return res.status(200).json({message: "Delete Successfully"});
    });
}


exports.requestReport = function (req, res, next) {
    const sellerId = 'A1LWZ980X488GK';
    amazonMws.reports.search({
        'Version': '2009-01-01',
        'Action': 'RequestReport',
        'SellerId': 'A1LWZ980X488GK',
        'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
        // 'MarketplaceIdList.Id.1': 'ATVPDKIKX0DER',
        'ReportType': '_GET_MERCHANT_LISTINGS_ALL_DATA_',
    }, function (error, response) {
        if (error) {
            console.log('error ', error);
        }
        if(response && response['ReportRequestInfo']){
            const reportRequestId = response['ReportRequestInfo']['ReportRequestId'];
            // Console Report Request Id
            console.log('request report done with id: ', reportRequestId);
            setTimeout(function () {
                console.log('currently on the timeout function in');
                amazonMws.reports.search({
                    'Version': '2009-01-01',
                    'Action': 'GetReportList',
                    'SellerId': sellerId,
                    'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8'
                    //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
                }, function (error, response) {
                    if (error) {
                        console.log('error ', error);
                    }
                    if(response && response['ReportInfo'] && response['ReportInfo'].length){
                        console.log('currently on the response report list');
                        const requestReportData = response['ReportInfo'].filter(function (rf) {
                            return rf['ReportRequestId'] === reportRequestId;
                        });
                        if( requestReportData && requestReportData.length ){
                            const reportId = requestReportData[0]['ReportId'];
                            // Console Report Id
                            console.log('report Id found with: ', reportId);
                            amazonMws.reports.search({
                                'Version': '2009-01-01',
                                'Action': 'GetReport',
                                'SellerId': sellerId,
                                'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
                                'ReportId': reportId
                            }, function (error, response) {
                                if (error) {
                                    console.log('error', error);
                                }
                                let respData = response.data || [];
                                let productData = {};
                                // Console Report Data
                                console.log('found report output data: ', respData.length);
                                async.each(respData, function (product, callb) {
                                    Product.findOne({ product_id: product['product-id'] }, (err, existingItem) => {
                                        if (err) { console.log(err);}

                                        if (existingItem) {
                                            return callb();
                                        }else{
                                            
                                            // scrapping image from product
                                            let asin = product['asin1'];
                                            scraper.init('http://www.amazon.com/gp/product/'+asin+'/', function(data){
                                                productData = {item_name: product['item-name'], item_description: product['item-description'], listing_id: product['listing-id'],
                                                    seller_sku: product['seller-sku'],
                                                    price: product['price'], quantity: product['quantity'], open_date: product['open-date'],
                                                    item_is_marketplace: product['item-is-marketplace'] === 'y',
                                                    product_id_type: product['product-id-type'], zshop_shipping_fee: product['zshop-shipping-fee'],
                                                    item_note: product['item-note'], item_condition: product['item-condition'],
                                                    zshop_category1: product['zshop-category1'], zshop_browse_path: product['zshop-browse-path'],
                                                    zshop_storefront_feature: product['zshop-storefront-feature'],
                                                    asin1: product['asin1'], asin2: product['asin2'], asin3: product['asin3'],
                                                    will_ship_internationally: product['will-ship-internationally'],
                                                    expedited_shipping: product['expedited-shipping'], zshop_boldface: product['zshop-boldface'], product_id: product['product-id'],
                                                    bid_for_featured_placement: product['bid-for-featured-placement'], add_delete: product['add-delete'],
                                                    pending_quantity: product['pending-quantity'],
                                                    fulfillment_channel: product['fulfillment-channel'], merchant_shipping_group: product['merchant-shipping-group'],
                                                    status: product['status']};

                                                productData.sellerId = sellerId;
                                                productData.image_url = data.image;
                                                var rating = data.review.length > 0 ? data.review[0].split(' ')[0]: 0;
                                                rating = Math.round(rating);
                                                productData.rating = rating;

                                                var pen_name = data.pen_name.length > 0 ? data.pen_name[0]: '';
                                                
                                                productData.pen_name = pen_name;

                                                const productDataObj = new Product(productData);
                                                productDataObj.save((err, savedItem) => {
                                                    if (err) { console.log(err); }
                                                    return callb();
                                                });
                                            });

                                            
                                        }
                                    });

                                }, function (err) {
                                    if(err){
                                        console.log(err);
                                    }else{
                                        // Console Report Done
                                        console.log('Already added whatever found product data');
                                        Product.find().limit(150).exec(function (err, products) {
                                            if (err){console.log(err);}
                                            return res.status(200).json({ products: products });
                                        })
                                    }
                                });
                            });
                        }
                    }
                });
            }, 120000)
        }

        // return res.status(200).json(response);
        // console.log('response', response);
    });
}


exports.saveProductReport = function (req, res, next) {
    let reportId = req.params.id;
    let sellerId = 'A1LWZ980X488GK';
    amazonMws.reports.search({
        'Version': '2009-01-01',
        'Action': 'GetReport',
        'SellerId': sellerId,
        'MWSAuthToken': 'amzn.mws.3aaf2cf5-417d-c970-3895-30d590fa88f8',
        'ReportId': reportId
        //'ReportTypeList.Type.1': 'REPORT_TYPE_LIST' //optional
    }, function (error, response) {
        if (error) {
            console.log(error);
        }
        // return res.status(200).json(response);
        let respData = response.data || [];
        let productData = {};
        // return res.status(200).json(response);
        async.each(respData, function (product, callb) {
            Product.findOne({ product_id: product['product-id'] }, (err, existingItem) => {
                if (err) { console.log(err);}

                if (existingItem) {
                    return callb();
                }else{
                    // scrapping image from product
                    let asin = product['asin1'];
                    scraper.init('http://www.amazon.com/gp/product/'+asin+'/', function(data){
                        productData = {item_name: product['item-name'], item_description: product['item-description'], listing_id: product['listing-id'],
                            seller_sku: product['seller-sku'],
                            price: product['price'], quantity: product['quantity'], open_date: product['open-date'],
                            item_is_marketplace: product['item-is-marketplace'] === 'y',
                            product_id_type: product['product-id-type'], zshop_shipping_fee: product['zshop-shipping-fee'],
                            item_note: product['item-note'], item_condition: product['item-condition'],
                            zshop_category1: product['zshop-category1'], zshop_browse_path: product['zshop-browse-path'],
                            zshop_storefront_feature: product['zshop-storefront-feature'],
                            asin1: product['asin1'], asin2: product['asin2'], asin3: product['asin3'],
                            will_ship_internationally: product['will-ship-internationally'],
                            expedited_shipping: product['expedited-shipping'], zshop_boldface: product['zshop-boldface'], product_id: product['product-id'],
                            bid_for_featured_placement: product['bid-for-featured-placement'], add_delete: product['add-delete'],
                            pending_quantity: product['pending-quantity'],
                            fulfillment_channel: product['fulfillment-channel'], merchant_shipping_group: product['merchant-shipping-group'],
                            status: product['status']};

                        productData.sellerId = sellerId;
                        productData.image_url = data.image;
                        var rating = data.review.length > 0 ? data.review[0].split(' ')[0]: '';
                        rating = Math.round(rating);
                        productData.rating = rating;

                        const productDataObj = new Product(productData);
                        productDataObj.save((err, savedItem) => {
                            if (err) { console.log(err); }
                            return callb();
                        });
                    });
                }
            });

        }, function (err) {
            if(err){
                console.log(err);
                console.log('there is a problem to get order data from API.');
            }else{
                return res.status(200).json(response);
                // console.log('Item Order Saved.');
            }
        });
    });
}



exports.list = function (req, res, next) {
    Product.find().limit(150).exec(function (err, products) {
        if (err){console.log(err);}
        Product.find(
            { $or: [{ rating: "1" }, { rating: "2"}] }
        ).limit(150).exec(function(err, reviews) {
            if (err){console.log(err);}
            
            console.log("review "+ reviews.length)
            return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
        });
        
    })
    
}

exports.filterList = function(req, res, next) {
    let date_start = req.body.date_start;
    let date_end = req.body.date_end;
    let filter_items = req.body.filter_items;
    let pen_name = req.body.pen_name;
  

    var d = new Date();
    var hrs = d.getHours();
    var min = d.getMinutes();

    if(date_start && date_end){
        date_start = date_start.replace('/', '-');
        date_start = date_start+ ' '+hrs+':'+min+':00 PDT';

        date_end = date_end.replace('/', '-');
        date_end = date_end+ ' '+hrs+':'+min+':00 PDT';
    }
    

    Product.find(
            { $or: [{ rating: "1" }, { rating: "2"}] }
        ).limit(150).exec(function(err, reviews) {
            if (err){console.log(err);}

            if ((date_start && date_end) && (filter_items && filter_items.length) && pen_name) {

                Product.find(
                  {
                    $and: [
                      {
                        open_date: {
                          $gte: date_start, $lt: date_end
                        }
                      },
                      {
                        $or: [
                          {
                            rating: {
                              $in: filter_items
                            }
                          },
                          {
                            pen_name: { $regex: '.*' + pen_name + '.*' }
                          }
                        ]
                      }
                    ]
                  }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
                });
            }else if ((date_start && date_end) && (filter_items && filter_items.length)) {
                Product.find(
                  {
                    $and: [
                      {
                        open_date: {
                          $gte: date_start, $lt: date_end
                        }
                      },
                      {
                        $or: [
                          {
                            rating: {
                              $in: filter_items
                            }
                          }
                        ]
                      }
                    ]
                  }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
                });
            } else if ((date_start && date_end) && pen_name) {
                Product.find(
                  {
                    $and: [
                      {
                        open_date: {
                          $gte: date_start, $lt: date_end
                        }
                      },
                      {
                        pen_name: { $regex: '.*' + pen_name + '.*' }
                      }
                    ]
                  }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
                });
            } else if (date_start && date_end) {
                Product.find(
                  { open_date: { $gte: date_start, $lt: date_end } }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}

                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
                });
            }else if ((filter_items && filter_items.length) && pen_name) {
                Product.find(
                    {
                    $and: [
                      {
                        pen_name: { $regex: '.*' + pen_name + '.*' }
                      },
                      {
                        $or: [
                          {
                            rating: {
                              $in: filter_items
                            }
                          }
                        ]
                      }
                    ]
                  }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });

                });

            } else if (filter_items && filter_items.length) {
                Product.find(
                  { $or: [{ rating: { $in: filter_items } }] }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });

                });

            } else if (pen_name) {
                Product.find(
                  { pen_name: { $regex: '.*' + pen_name + '.*' }  }
                ).limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });

                });

            } else {
                Product.find().limit(150).exec(function(err, products) {
                    if (err){console.log(err);}
                    return res.status(200).json({ totalNegativeReview: reviews.length, products: products });
                });
            }
    });
};



function formatProductData(resp){
    let orderData = {NextToken: resp.NextToken, RequestId: resp.ResponseMetadata.RequestId};
    orderData.orders = resp.Orders.Order;
    return orderData;
}
