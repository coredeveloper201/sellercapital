const config = require("../config/main");
const Template = require("../models/template");
const sendGrid = require("../config/sendgrid");
// Find Hostname
const os = require("os");
const hostName = os.hostname();
const SERVER_STATIC_URL = 'http://' + hostName + ':9001';

// TO Do 
// Attach file if file is included with template 

function sendEmailBody(templateData){
  var logo = "https://reviewkick.s3.amazonaws.com/uploads/ckeditor/pictures/15/content_amazon-logo.png";
  if(templateData.logo){
    logo = SERVER_STATIC_URL + templateData.logo;
  }
  var email_attachment = '';
  if(templateData.email_attachment){

    var attachment = SERVER_STATIC_URL + templateData.email_attachment;
    var attachment_name = templateData.email_attachment.split('/')[2];
    email_attachment = `There is an attachment with this email. Click this link to view the attachment. 
                          <a href="${attachment}" target="_blank">${attachment_name}</a>
                      `;
  }
  let htmlBody = `
        <table bgcolor="#f2f2f2" border="0" cellpadding="0" cellspacing="0" width="100%">
          <tbody>
            <tr>
              <td align="center" style="padding: 30px 34%;">
                <img class="fr-fic" src="${logo}" style="display: block; height: 77.3333px; margin: 5px auto; vertical-align: top; width: 160px;">
              </td>
            </tr>
            <tr>
              <td class="template_area" style="padding: 5px;">
                ${templateData.email_message}
              </td>
            </tr>
            <tr>
              <td align="center">&nbsp;</td>
            </tr>
            <tr>
              <td style="padding: 15px 5px;">${email_attachment}</td>
            </tr>
          </tbody>
        </table>
    `;
  return htmlBody;
}

exports.sendTestMail = function(req, res, next) {
  let templateId = req.body.id;
  Template.findById(templateId, function(err, templateData) {
    if (err) {
      return res.send(err);
    }
    const messsageData = {
      to: req.body.email,
      from: "info@sellercapital.com",
      subject: templateData.email_subject,
      html: sendEmailBody(templateData)
    };
    sendGrid.sendEmail(messsageData);
    setTimeout(function() {
      return res.status(200).json({ message: "The Message has been sent !" });
    }, 1000);
  });
};


exports.resentWelcomeMail = function(req, res, next) {

  let name = req.body.firstName;
  let email = req.body.email;
  let appUrl;
  if (req.hostname) {
    appUrl = "http://" + req.hostname + ":3000/#/dashboard";
  } else {
    appUrl = "http://" + req.hostname + ":3000/#/dashboard";
  }

  // to do
  const activationUrl = SERVER_STATIC_URL+"/api/active_account/" + req.body._id;

  let htmlBody = `
Hi ${name},<br>
<h3>Welcome, thank you for choosing Seller Capital.</h3>

<p>Please click on the button bellow to complete your account activation.</p>

<a class="" href="${activationUrl}" style="background-color:darkgreen; color: #ffffff; padding: 5px 10px;">Activation Account</a>

<br>
Sincerely, <br/>
Seller Capital Team
 `;

  const messageData = {
    to: email,
    from: "info@sellercapital.com",
    subject: "Account activation link - Seller Capital",
    html: htmlBody
  };
  sendGrid.sendEmail(messageData);
  setTimeout(function() {
    console.log("message has been sent");
    return res.status(200).json({ message: "The Message has been sent !" });
    // res.redirect(appUrl);
  });
};



// Sending template to orderer instantly


exports.sendToOrderer = function(obj,next) {

    const messsageData = {
        to: obj.email,
        from: "info@sellercapital.com",
        subject: obj.templateData.email_subject,
        html: sendEmailBody(obj.templateData)
    };
    sendGrid.sendEmail(messsageData);
    next(true)

}
