import {API_URL, CLIENT_ROOT_URL, FETCH_ORDERS, FETCH_SINGLE_ORDER, ORDER_ERROR} from "./types";
import {FETCH_TEMPLATES, FETCH_SINGLE_TEMPLATE, TEMPLATE_ERROR, MAIL_ERROR, SEND_MAIL} from "./types";

// for crud operation
import { getData, postData, putData, deleteData, putDataFetchAPI, postDataMultiPart, putDataFetchAPIMultiPart } from './index';


export function fetchTemplates(id = null) {
    const url = '/templates';
    const type = {
      actionType: FETCH_TEMPLATES,
      errorType: TEMPLATE_ERROR,
      loaderType: null,
    }
    return function (dispatch) {
        getData(type, true, url, function (respData) {
            return dispatch(respData);
        })
    }
}


export function tCreationSubmit(formValues) {
    setTimeout(function () {
        return true;
    }, 2000)
    const url = '/create_template';
    const type = {
      actionType: FETCH_TEMPLATES,
      errorType: TEMPLATE_ERROR,
      loaderType: null,
    }
    return dispatch => postDataMultiPart(type, true, url, dispatch, formValues);
}

export function tModifySubmit(formValues) {
    setTimeout(function () {
        return true;
    }, 2000)
    const url = '/modify_template';
    const type = {
      actionType: FETCH_TEMPLATES,
      errorType: TEMPLATE_ERROR,
      loaderType: null,
    }
    return dispatch => putDataFetchAPIMultiPart(type, true, url, dispatch, formValues);
}

export function sendTestMail(formValues) {
    setTimeout(function () {
        return true;
    }, 2000)
    const url = '/send-test-template-mail'
    const type = {
      actionType: SEND_MAIL,
      errorType: MAIL_ERROR,
      loaderType: null,
    }
    return dispatch => postData(type, true, url, dispatch, formValues);
}

export function tDeleteSubmit(id) {
    setTimeout(function () {
        return true;
    }, 2000)
    const url = '/delete_template/' + id;
    const type = {
      actionType: FETCH_TEMPLATES,
      errorType: TEMPLATE_ERROR,
      loaderType: null,
    }
    return dispatch => deleteData(type, true, url, dispatch);
}

// export const REDUCER = 'order'
