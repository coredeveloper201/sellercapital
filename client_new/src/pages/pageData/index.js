import moment from 'moment'
let fulfillmentChannelOptionKeys = [
  {
    title: "AFN",
    value: "AFN"
  },
  {
    title: "MFN",
    value: "MFN"
  }
]
let orderOptionKeys = [
  {
    title: "Shipped",
    value: "Shipped"
  },
  {
    title: "Pending",
    value: "Pending"
  },
  {
    title: "Unshipped",
    value: "Unshipped"
  },
  {
    title: "Delivered",
    value: "Delivered"
  },
  {
    title: "Canceled",
    value: "Canceled"
  },
  {
    title: "Returned",
    value: "Returned"
  }
]
let orderEmailsOptionKeys = [
  {
    title: 'Has Sent',
    value:  'Has Sent'
  },
  {
    title: 'Not Sent',
    value:  'not Sent'
  },
  {
    title: 'Is Queued',
    value:  'is queued'
  },
  {
    title: 'Not Queued',
    value:  'Not Queued'
  },
]

let reviewOptionKeys = [
  {
    value: "1",
    title: "1 star rating",
  },
  {
    value: "2",
    title: "2 star rating",
  },
  {
    value: "3",
    title: "3 star rating",
  },
  {
    value: "4",
    title: "4 star rating",
  },
  {
    value: "5",
    title: "5 star rating",
  },
]
let filteringOptionsFeedback = [
  {
    title: "Reviews",
    keys: reviewOptionKeys,
  }
]
let filteringOptions = [
  {
    title: "Fulfillment Channel",
    keys: fulfillmentChannelOptionKeys
  },
  {
    title: "Order Status",
    keys: orderOptionKeys
  },
  {
    title: "Email Status",
    keys: orderEmailsOptionKeys
  }
]
let filteringOptionsProduct = [
  {
    title: "Product By review",
    keys: reviewOptionKeys
  },
  {
    title: "Email Status",
    keys: orderEmailsOptionKeys,
  },
]


// let feedbackSelectKeysReactSelect  = [
//   {
//     label: '1 star feedback',
//     value: {feedback_rating: 1},
//     valueKey: 1,
//   },
//   {
//     label: '2 star feedback',
//     value: {feedback_rating: 2},
//     valueKey: 2,
//   },
//   {
//     label: '3 star feedback',
//     value: {feedback_rating: 3},
//     valueKey: 3,
//   },
//   {
//     label: '4 star feedback',
//     value: {feedback_rating: 4},
//     valueKey: 4,
//   },
//   {
//     label: '5 star feedback',
//     value: {feedback_rating: 5},
//     valueKey: 5,
//   },

// ]
// let reviewSelectKeysReactSelect  = [
//   {
//     label: '1 star review',
//     value: {review_rating: 1},
//     valueKey: 6,
//   },
//   {
//     label: '2 star review',
//     value: {review_rating: 2},
//     valueKey: 7,
//   },
//   {
//     label: '3 star review',
//     value: {review_rating: 3},
//     valueKey: 8,
//   },
//   {
//     label: '4 star review',
//     value: {review_rating: 4},
//     valueKey: 9,
//   },
//   {
//     label: '5 star review',
//     value: {review_rating: 5},
//     valueKey: 10,
//   },
// ]
// let promotionSelectKeysReactSelect  = [
//   {
//     label: 'With item discount',
//     value: {promotion: 'item_discount'},
//     valueKey: 11,
//   },
//   {
//     label: 'With shipping discount',
//     value: {promotion: 'shipping_discount'},
//     valueKey: 12,
//   },
// ]

// let otherSelectKeysReactSelect  = [
//   {
//     label: 'Returns',
//     value: {other: 'returns'},
//     valueKey: 13,
//   },
//   {
//     label: 'Repeat Buyers',
//     value: {other: 'repeat_buyers'},
//     valueKey: 14,
//   }
// ]

// let excludeOrderKeysReactSelect = [
//   {
//     label: "Feedback",
//     options: feedbackSelectKeysReactSelect
//   },
//   {
//     label: "Reviews",
//     options: reviewSelectKeysReactSelect
//   },
//   {
//     label: "Promotions",
//     options: promotionSelectKeysReactSelect
//   },
//   {
//     label: "Others",
//     options: otherSelectKeysReactSelect
//   },

// ]


let excludeOrderKeys = [
  {
    title: "Feedback",
    keys: [
      {
        label: "1 star feedback",
        value: 'feedback:1',
        optGroup: 'feedback',
      },
      {
        label: "2 star feedback",
        value: 'feedback:2',
        optGroup: 'feedback',
      },
      {
        label: "3 star feedback",
        value: 'feedback:3',
        optGroup: 'feedback',
      },
      {
        label: "4 star feedback",
        value: 'feedback:4',
        optGroup: 'feedback',
      },
      {
        label: "5 star feedback",
        value: 'feedback:5',
        optGroup: 'feedback',
      },
    ]
  },
  {
    title: "Reviews",
    keys: [
      {
        label: "1 star review",
        value: 'review:1',
        optGroup: 'review',
      },
      {
        label: "2 star review",
        value: 'review:2',
        optGroup: 'review',
      },
      {
        label: "3 star review",
        value: 'review:3',
        optGroup: 'review',
      },
      {
        label: "4 star review",
        value: 'review:4',
        optGroup: 'review',
      },
      {
        label: "5 star review",
        value: 'review:5',
        optGroup: 'review',
      },

    ]
  },
  {
    title: "Promotions",
    keys: [
      {
        label: "With item discount",
        value: 'promotion:item_discount',
        optGroup: 'promotion',
      },
      {
        label: "With shipping discount",
        value: 'promotion:shipping_discount',
        optGroup: 'promotion',
      },
    ]
  },
  {
    title: "Other",
    keys: [
      {
        label: "Returns",
        value: 'other:return',
        optGroup: 'other',
      },
      {
        label: "Repeat Buyers",
        value: 'other:repeat_buyer',
        optGroup: 'Other',
      }

    ]
  }
];

function randomColor (colors) { return colors[ parseInt(Math.random() * colors.length) ] }

/**
 * transform analysis data which coming from api call
 * @param  {[arr]} options.analysisData  [it expect a array. which will be sorted and generated chart data and label]
 * @param  {[string]} options.key   [it will be key of the data]
 * @param  {[string]} options.label [label name. Usually when user hover on certain bar point it will pop up with respective data]
 * @return {[array]}  graphData              [it will generate for final transformed data which will be main data value for chart]
 */
function transformAnalysisData ({analysisData, key, label, chartName }) {
  // let analysisData = [{"_id":"2017-12","totalFeedback":2},{"_id":"2017-09","totalFeedback":6},{"_id":"2018-01","totalFeedback":4},{"_id":"2017-08","totalFeedback":2},{"_id":"2018-04","totalFeedback":2},{"_id":"2017-11","totalFeedback":6},{"_id":"2017-10","totalFeedback":10},{"_id":"2018-03","totalFeedback":2},{"_id":"2018-02","totalFeedback":6}]
  var materialColor = ['#e53935', '#d32f2f', '#c62828', '#b71c1c', '#d81b60', '#c2185b', '#ad1457', '#880e4f', '#8e24aa', '#7b1fa2', '#6a1b9a', '#4a148c', '#5e35b1', '#512da8', '#4527a0', '#311b92', '#3949ab', '#303f9f', '#283593', '#1a237e', '#1e88e5', '#1976d2', '#1565c0', '#0d47a1', '#039be5', '#0288d1', '#0277bd', '#01579b', '#00acc1', '#0097a7', '#00838f', '#006064', '#00897b', '#00796b', '#00695c', '#004d40', '#43a047', '#388e3c', '#2e7d32', '#1b5e20', '#7cb342', '#689f38', '#558b2f', '#33691e', '#c0ca33', '#afb42b', '#9e9d24', '#827717', '#fbc02d', '#f9a825', '#f57f17', '#ffb300', '#ffa000', '#ff8f00', '#ff6f00', '#fb8c00', '#f57c00', '#ef6c00', '#e65100', '#f4511e', '#e64a19', '#d84315', '#bf360c', '#6d4c41', '#5d4037', '#4e342e', '#3e2723', '#757575', '#616161', '#424242', '#212121', '#546e7a', '#455a64', '#37474f', '#263238',]
  var flatColor = ["#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", "#f1c40f", "#e67e22", "#e74c3c", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#7f8c8d", ];
  let labels = []
  let data = []
  let borderColor = []
  let backgroundColor = []
  let analysisDataSorted = analysisData.sort((a, b) => {
    let c = moment(a._id, 'YYYY/MM');
    let d = moment(b._id, 'YYYY/MM');
    return new Date(c) - new Date(d)
  })
  analysisDataSorted.forEach(obj => {
    var check = moment(obj._id, 'YYYY/MM');
    var month = check.format('MMM');
    var year  = check.format('YYYY');
    var str = `${month} ${year}`
    labels.push(str)
    data.push(obj[key])
    if (chartName == 'bar') {
      borderColor.push(randomColor(flatColor))
      backgroundColor.push(randomColor(materialColor))
    } else if (chartName == 'line') {
      borderColor = randomColor(materialColor)
      backgroundColor = randomColor(flatColor)
    }
  })
  let dataSet = {
    label: label,
    backgroundColor,
    borderColor,
    data,
    fill: true,
  }
  // if (chartName == 'line') {
  //   dataSet = {...dataSet, ...{fill: true}}
  // }

  let graphData = {
    labels,
    datasets: [dataSet]
  }
  return graphData;
}


const PageData = {
  filteringOptions,
  filteringOptionsFeedback,
  filteringOptionsProduct,
  excludeOrderKeys,
  transformAnalysisData,
}

export default PageData;
// transformAnalysisData
