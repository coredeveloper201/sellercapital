import React from 'react';
import {Spin} from 'antd'
import {Bar} from 'react-chartjs-2';
import pageData from '../pageData/index.js'
console.log('pageData', pageData)
export default class DashboardFeedbackChart extends React.Component {
  render () {
    const {
      feedbackAnalysisLoader,feedbackAnalysis} = this.props
    let barOptions = {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            padding: 25,
          }
        }]
      }
    }
    let feedbackAnalysisData = pageData.transformAnalysisData({
        analysisData: feedbackAnalysis,
        key: 'totalFeedback',
        label: 'Total Feedback',
        chartName: 'bar',
      })
    return feedbackAnalysisLoader ?
    <div className="text-center" >
      <Spin />
    </div> : ( feedbackAnalysis.length > 1 ?
    <Bar
        options={barOptions}
        data={feedbackAnalysisData}
        height={400}
      /> : <h2>No feedback found</h2>
      )


  }

}
