import React from "react";
import {
  Table,
  Icon,
  Input,
  Button,
  DatePicker,
  Select,
  Radio,
  notification,
  Alert,
  Modal,
  Rate,
} from "antd";
import moment from "moment";
import { connect } from "react-redux";
import tableData from "./data.json";
// import { fetchOrders } from './../../../ducks/filterOrder'
import { fetchProducts, requestForUpdateReport, fetchProductFilters } from "./../../../ducks/product";
import { setLoading } from "./../../../ducks/app";
import "./style.scss";
import { REDUCER, submit } from "../../../ducks/profile";
import pageData from "../../pageData";
import ReactSelect from 'react-select'
import Animated from 'react-select/lib/animated';

const RangePicker = DatePicker.RangePicker;
const { Option, OptGroup } = Select;
const confirm = Modal.confirm;

let fulfillmentNetworkOptions = [
  {
    label: "AFN",
    value: "AFN",
    key: "fulfillment_channel",
  },
  {
    label: "MFN",
    value: "MFN",
    key: "fulfillment_channel",
  }
]
let emailStatusOptions = [
  {
    label: 'Has Sent',
    value:  'Has Sent',
    key: 'email_status',
  },
  {
    label: 'None Sent',
    value:  'none Sent',
    key: 'email_status',
  },
  {
    label: 'Is Queued',
    value:  'is queued',
    key: 'email_status',
  },
  {
    label: 'None Queued',
    value:  'None Queued',
    key: 'email_status',
  },
]
const options = [
  {
    label: 'Fulfillment Network',
    options: fulfillmentNetworkOptions,
  },
  {
    label: 'Email Status',
    options: emailStatusOptions,
  }
]
 let reactSelectForFutureUse = `<div className="filter_area">
            <ReactSelect
              onChange={(selectedOption) => this.setState({selectedOption})}
              isMulti
              value={this.state.selectedOption}
              options={options}
            />
          </div>`


// for crud operation
// import { getData, postData, putData, deleteData } from './index';

// import * as actions from 'ducks/order'

// const mapStateToProps = (state, props) => ({
//     orderData: state.order.orders[REDUCER],
// })

// const mapStateToProps = (state, ownProps) => {
//     return {
//         orderData : state.order[orders]
//     };
// }
//
// @connect(mapStateToProps)


// function mapStateToProps(state) {
//     return {
//         orders: state.order.orders,
//     };
// }


// const { dispatch } = this.props;
// console.log(this.props);
const defaultPagination = {
  pageSizeOptions: ["50", "100", "250"],
  showSizeChanger: true,
  current: 1,
  size: "small",
  showTotal: (total: number) => `Total ${total} items`,
  total: 0, pageSize: 50
};

let allFilters = pageData.filteringOptionsProduct.map((option, i) =>
  <OptGroup key={i} label={option.title}>
    {
      option.keys.map((op, j) =>
        <Option key={op.value} value={op.value}>{op.title}</Option>
      )
    }
  </OptGroup>
);

const children = [];
let optionKeys = [{ key: "AFN", value: "AFN" }, { key: "MFN", value: "MFN" }, { key: "Shipped", value: "Shipped" },
  { key: "Pending", value: "Pending" }, { key: "Unshipped", value: "Unshipped" },
  { key: "Delivered", value: "Delivered" }, { key: "Canceled", value: "Canceled" }, {
    key: "Returned",
    value: "Returned"
  }];
for (let i = 0; i < optionKeys.length; i++) {
  children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
}

// const orderData = fetchOrders();

// console.log(dispatch(orders()).data);


const mapStateToProps = (state, props) => ({
  products: state.product.products,
  totalNegativeReview: state.product.totalNegativeReview,
});


const mapDispatchToProps = (dispatch, props) => ({
  fetchProducts: dispatch(fetchProducts()),
  getUpdateProduct: () => {
    dispatch(requestForUpdateReport());
  },
  fetchProductFilters: (filter) => {
    dispatch(fetchProductFilters(filter))
  }
});


@connect(mapStateToProps, mapDispatchToProps)

class Orders extends React.Component {

  componentWillMount() {
    // Fetch inbox (conversations involving current user)
    // this.props.fetchOrders();
    // setLoading(true);
  }

  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: "",
    filtered: false,
    loading: true,
    selectedOption: null,
    pen_name: '',
  };

  setUpdatedProduct = (e) => {
    const self = this;
    confirm({
      title: "Are you sure to sent request to get report?",
      content: "It will sent a request to MWS for get products, after getting the product will store on the database",
      onOk() {
        self.props.getUpdateProduct();
        setTimeout(function() {
          notification.open({
            message: "Product Report",
            description: "A report requested sent to MWS to get updated products, It will take around few minutes to get update"
          });
        }, 2000);
      },
      onCancel() {
        console.log("Cancel");
      }
    });
  };

  onClickFilterSearch = e => {
    const { date_start, date_end, filter_items, pen_name } = this.state;
    let filter_data = this.props.fetchProductFilters({ date_start, date_end, filter_items, pen_name });

    console.log(filter_data);
    // console.log(this.props.filterOrder);
  };

  onDatePickerChange = (date, dateString) => {
    if (dateString && dateString.length) {
      this.setState({ date_start: dateString[0], date_end: dateString[1] });
    }
  };

  filterItemChange = values => {
    this.setState({ filter_items: values });
  };


  handleSizeChange = (e) => {
    this.setState({ size: e.target.value });
  };

  onInputChange = e => {
    this.setState({ searchText: e.target.value });
  };


  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager };
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize;
        pager.pageSize = pagination.pageSize;
        pager.current = 1;
      } else {
        pager.current = pagination.current;
      }
      this.setState({
        pager: pager
      });
    }
  };

  _generateNegativeReviewDescription = numberOfNReview =>  numberOfNReview > 1 ? `You have ${numberOfNReview} negative reviews` : `You have ${numberOfNReview} negative review`


  render() {
    console.log('this.props', this.props)
    let imageStyle = {
      maxWidth: 100,
      maxHeight: 100,
      width: 'auto',
      height: 'auto',
    }

    let { pager, data, tableData, pen_name } = this.state;
    if (this.props.products && this.props.products.length) {
      data = this.props.products;
      tableData = this.props.products;
      console.log(tableData);
      this.state.loading = false;
    }
    const columns = [
      {
        title: "ID",
        dataIndex: "product_id",
        key: "product_id",
        sorter: (a, b) => a.product_id - b.product_id
      },
      {
        title: "ASIN",
        dataIndex: "asin1",
        key: "asin1"
        // sorter: (a, b) => a.asin1 - b.asin1
      },
      {
        title: "Status",
        dataIndex: "status",
        key: "status"
        // sorter: (a, b) => a.status - b.status
      },
      {
        title: "Open Date",
        dataIndex: "open_date",
        key: "open_date"
        // sorter: (a, b) => a.open_date - b.open_date
      },
      {
        title: "Title",
        dataIndex: "item_name",
        key: "item_name",
        // sorter: (a, b) => a.item_name - b.item_name,
        render: (text, row) =>
        <div className="d-flex">
          <img className="mr-2" style={imageStyle} src={row.image_url} alt=""/>
          <span className="product_title">{text} </span>
        </div>
      },
      {
        title: "Rating",
        dataIndex: "rating",
        key: "rating",
        className: "rating_data",
        render: text => (
          <span className="rating_star"><Rate value={Number(text)} disabled={true} allowHalf={true}/></span>)
      },
      {
        title: "Pen Name",
        dataIndex: "pen_name",
        key: "pen_name",
        className: "pen_name_data",
      },
      {
        title: "Price",
        dataIndex: "price",
        key: "price",
        sorter: (a, b) => a.price - b.price,
        render: text => (<span className={"product_price"}>{text}</span>)

      },

    ];

    return (
      <div className="card">
        <div className="card-body">
        {
          this.props.totalNegativeReview ?
          <div className="filter_area mb-2">
            <Alert
              message="Negative Reviews"
              description={this._generateNegativeReviewDescription(this.props.totalNegativeReview)}
              type="error"
              closable
            />
          </div> : null
        }
          <div className="filter_area">
            <div className="filter_submit_btn">
              <Button type="primary" onClick={this.setUpdatedProduct}><i className="icmn-cogs mr-1"/> Generate Report To
                Get Updated Product</Button>
            </div>
          </div>


          <div className="filter_area d-md-flex">
            <div className="date_picker_area m-2">
              <RangePicker
                ranges={{
                  Today: [moment(), moment()],
                  "This Month": [moment(), moment().endOf("month")],
                  "Last 3 Months": [moment().subtract(3, "months"), moment()],
                  "Last 6 Months": [moment().subtract(6, "months"), moment()],
                  "Last 1 Year": [moment().subtract(1, "year"), moment()]
                }}
                format="YYYY/MM/DD"
                onChange={this.onDatePickerChange}
              />
              <br/>
            </div>
            <div className="filter_selection m-2" style={{ minWidth: 250 }}>
              <Select
                mode="multiple"
                size="default"
                placeholder="Please select fields to filter"
                onChange={this.filterItemChange}
                style={{ width: "100%" }}>
                {allFilters}
              </Select>
            </div>

            <div className="filter_input m-2">
              <Input
                value={pen_name}
                onChange={(e) => this.setState({pen_name: e.target.value}) }
                placeholder="Enter pen name to search"
               />
            </div>

            <div className="filter_submit_btn m-2">
              <Button type="primary" onClick={this.onClickFilterSearch}>Search</Button>
            </div>
          </div>

          <Table
            columns={columns}
            dataSource={data}
            rowKey={record => record.key}
            pagination={pager}
            onChange={this.handleTableChange}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state) {
//     return {
//         orders: state.order.orders,
//     };
// }


// export default Orders

export default Orders;
