import React from 'react'
import {
  Table,
  Icon,
  Input,
  Button,
  DatePicker,
  Select,
  Radio } from 'antd'
import moment from 'moment';
import { connect } from 'react-redux'
import tableData from './data.json'
import pageData from '../../pageData'
import ids from 'short-id'
// import { fetchOrders } from './../../../ducks/filterOrder'
import { fetchOrders, fetchFilteredOrders } from './../../../ducks/order'
import {setLoading} from './../../../ducks/app'
import './style.scss'
import {REDUCER, submit} from "../../../ducks/profile";
const RangePicker = DatePicker.RangePicker;
const { Option, OptGroup } = Select;


// for crud operation
// import { getData, postData, putData, deleteData } from './index';

// import * as actions from 'ducks/order'

// const mapStateToProps = (state, props) => ({
//     orderData: state.order.orders[REDUCER],
// })

// const mapStateToProps = (state, ownProps) => {
//     return {
//         orderData : state.order[orders]
//     };
// }
//
// @connect(mapStateToProps)





// function mapStateToProps(state) {
//     return {
//         orders: state.order.orders,
//     };
// }


// const { dispatch } = this.props;
// console.log(this.props);
const defaultPagination = {
  pageSizeOptions: ['50', '100', '250'],
  showSizeChanger: true,
  current: 1,
  size: 'small',
  showTotal: (total: number) => `Total ${total} items`,
  total: 0, pageSize: 50
}


let allFilters = pageData.filteringOptions.map( (option, i) =>
    <OptGroup key={i} label={option.title}>
      {
        option.keys.map((op, j) =>
          <Option key={op.value} value={op.value}>{op.title}</Option>
        )
      }
    </OptGroup>
  )

// const children = [];
// let optionKeys = [{key: "AFN", value: "AFN"},{key: "MFN", value: "MFN"},{key: "Shipped", value: "Shipped"},
//     {key: "Pending", value: "Pending"},{key: "Unshipped", value: "Unshipped"},
//     {key: "Delivered", value: "Delivered"},{key: "Canceled", value: "Canceled"},{key: "Returned", value: "Returned"}];
// for ( let i = 0; i < optionKeys.length; i++ ) {
//     children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
// }

// const orderData = fetchOrders();

// console.log(dispatch(orders()).data);


const mapStateToProps = (state, props) => ({
    orders: state.order.orders
})


const mapDispatchToProps = (dispatch, props) => ({
    fetchOrders: dispatch(fetchOrders()),
    filterOrder: filter => {
        dispatch(fetchFilteredOrders(filter))
    }
})



@connect(mapStateToProps, mapDispatchToProps)

class Orders extends React.Component {

    componentWillMount() {
        // Fetch inbox (conversations involving current user)
        // this.props.fetchOrders();
        // setLoading(true);
    }

  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: '',
    filtered: false,
      loading: true
  }

    onClickFilterSearch = e => {
        const {date_start, date_end, filter_items} = this.state;
        let filter_data = this.props.filterOrder({date_start, date_end, filter_items});
        this.setState({ loading: true });
    }

    onDatePickerChange = (date, dateString) => {
        if(dateString && dateString.length){
            this.setState({ date_start: dateString[0], date_end: dateString[1] });
        }
    }

    filterItemChange = values => {
        this.setState({ filter_items: values });
    }


    handleSizeChange = (e) => {
        this.setState({ size: e.target.value });
    }

  onInputChange = e => {
    this.setState({ searchText: e.target.value })
  }

  onSearch = () => {
    const { searchText, tableData } = this.state
    let reg = new RegExp(searchText, 'gi')
    this.setState({
      filterDropdownVisible: false,
      filtered: !!searchText,
      data: tableData
        .map(record => {
          let match = record.name.match(reg)
          if (!match) {
            return null
          }
          return {
            ...record,
              name: (
              <span>
                {record.name
                  .split(reg)
                  .map(
                    (text, i) =>
                      i > 0 ? [<span className="highlight">{match[0]}</span>, text] : text,
                  )}
              </span>
            ),
          }
        })
        .filter(record => !!record),
    })
  }

  handleTableChange = (pagination, filters, sorter) => {
    if (this.state.pager) {
      const pager = { ...this.state.pager }
      if (pager.pageSize !== pagination.pageSize) {
        this.pageSize = pagination.pageSize
        pager.pageSize = pagination.pageSize
        pager.current = 1
      } else {
        pager.current = pagination.current
      }
      this.setState({
        pager: pager,
      })
    }
  }

  render() {
    console.log('this.props inside orderpage', this.props)
    console.log('this.state inside orderpage', this.state)
    let productDefultImage = "https://picsum.photos/100"

    let { pager, data, tableData } = this.state;
      if (this.props.orders && this.props.orders.length) {
          data = this.props.orders;
          tableData = this.props.orders;
          console.log(tableData);
          this.state.loading = false;
      }
    const columns = [
      {
        title: 'Type',
        dataIndex: 'FulfillmentChannel',
        key: ids.generate(),
          className: 'order_type',
        render: text => (
          <span className={'b_type ' + text}>
            {text}
          </span>
        ),
      },
        {
            title: 'Order Date',
            dataIndex: 'PurchaseDate',
            key: ids.generate(),
            className: 'order_date',
            render: text => <span>{(new Date(text)).toLocaleDateString('en-US', { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'  })}</span>,
        },
      {
        title: 'Order ID',
        dataIndex: 'AmazonOrderId',
        key: ids.generate(),
        sorter: (a, b) => a.AmazonOrderId - b.AmazonOrderId,
          render: order_id => <a href={'https://sellercentral.amazon.com/hz/orders/details?_encoding=UTF8&orderId=' + order_id} target={'_blank'}>{order_id}</a>
      },
      {
        title: 'Buyer',
        dataIndex: 'BuyerName',
        key: ids.generate(),
      },
        {
            title: 'Product Details',
            dataIndex: 'orderItem',
            key: ids.generate(),
            className: 'product_details',
            render: (text, row) => text.map((tx, i)=> {
              return (<div className="d-flex align-items-center justify-content-center" key={i}>
                <div>
                  <span className="title">{tx && tx.title?tx.title:''}</span>
                  <span className="qty"><b>QTY:</b> {tx.qty}</span><span className="price"><b>PRICE:</b> {tx.price}</span>
                  <span className="asin"><b>ASIN:</b> {tx.asin}</span>
                  <span className="sku"><b>SKU:</b> {tx.sku}</span>
                </div>
              </div>)
            })
        },
      {
        title: 'Status',
        dataIndex: 'OrderStatus',
        key: ids.generate(),
        className: 'order_status',
        sorter: (a, b) => a.OrderStatus - b.OrderStatus,
      },
      {
        title: 'Number of email sent',
        dataIndex: 'number_of_email_sent',
        key: ids.generate(),
        className: 'number_of_email_sent',
        render: () => <span>0</span>
      },
      {
        title: 'Action',
        dataIndex: 'AmazonOrderId',
        key: ids.generate(),
        className: 'email_action',
        render: id => <Button type='primary'>Send email</Button>
      },
    ]

    return (
      <div className="card">
        <div className="card-header">
            <div className="filter_area">
                <div className="date_picker_area">
                    <RangePicker
                        ranges={{ Today: [moment(), moment()], 'This Month': [moment(), moment().endOf('month')], 'Last 3 Months': [moment().subtract(3, 'months'), moment()], 'Last 6 Months': [moment().subtract(6, 'months'), moment()] , 'Last 1 Year': [moment().subtract(1, 'year'), moment()] }}
                        format="YYYY/MM/DD"
                        onChange={this.onDatePickerChange}
                    />
                    <br />
                </div>
                <div className="filter_selection">
                    <Select
                        mode="multiple"
                        size="default"
                        placeholder="Please select fields to filter"
                        onChange={this.filterItemChange}
                        style={{ width: '100%' }}>
                        {allFilters}
                    </Select>
                </div>
                <div className="filter_submit_btn">
                    <Button type="primary" onClick={this.onClickFilterSearch}>Search</Button>
                </div>
            </div>
        </div>
        <div className="card-body">
          <Table
            columns={columns}
            dataSource={data}
            rowKey={record => record.key}
            pagination={pager}
            onChange={this.handleTableChange}
            loading={this.state.loading}
          />
        </div>
      </div>
    )
  }
}

// function mapStateToProps(state) {
//     return {
//         orders: state.order.orders,
//     };
// }


// export default Orders

export default Orders;
