import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import cookie from "react-cookie";
import { connect } from "react-redux";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";
import htmlToDraft from "html-to-draftjs";
import PropTypes from "prop-types";
import moment from "moment";
import Mustache from 'mustache';
import {
  Table,
  Button,
  Input,
  message,
  DatePicker,
  Select,
  Steps,
  Upload,
  Icon,
  Form,
  Dropdown,
  Modal,
  Row,
  Col,
  TimePicker
} from "antd";
import { fetchTemplates, tCreationSubmit, tModifySubmit, tDeleteSubmit } from "./../../../ducks/template";
import { fetchUser } from "./../../../ducks/index";
import { fetchOrders } from "./../../../ducks/order";
import "./style.scss";

// Editor Draft js
import { EditorState, convertToRaw, ContentState, Modifier } from "draft-js";
import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import CustomOption from "./CustomOption";
import TemplatePreviewModal from '../../pageComponents/TemplatePreviewModal'
import {SERVER_STATIC_URL} from '../../../ducks/types.js'
import pageData from '../../pageData/'



const Step = Steps.Step;


const RangePicker = DatePicker.RangePicker;
const {Option, OptGroup} = Select;
const FormItem = Form.Item;
const { TextArea } = Input;
const confirm = Modal.confirm;


const steps = [{
  title: "Template"
}, {
  title: "Campaign"
}];


const defaultPagination = {
  pageSizeOptions: ["20", "50", "100"],
  showSizeChanger: true,
  current: 1,
  size: "small",
  showTotal: (total: number) => `Total ${total} items`,
  total: 0, pageSize: 50
};


const children = [];
let optionKeys = [{ key: "AFN", value: "AFN" }, { key: "MFN", value: "MFN" }, { key: "Shipped", value: "Shipped" },
  { key: "Pending", value: "Pending" }, { key: "Unshipped", value: "Unshipped" },
  { key: "Delivered", value: "Delivered" }, { key: "Canceled", value: "Canceled" }, {
    key: "Returned",
    value: "Returned"
  }];
for (let i = 0; i < optionKeys.length; i++) {
  children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
}


const mapStateToProps = (state, props) => ({
  templates: state.template.templates,
  orders: state.order.orders,
  user: state.user.profile
});


const mapDispatchToProps = (dispatch, props) => ({
  fetchTemplates: dispatch(fetchTemplates()),
  fetchOrders: dispatch(fetchOrders()),
  templateCreateSubmit: values => dispatch(tCreationSubmit(values)),
  templateModifySubmit: values => dispatch(tModifySubmit(values)),
  deleteModifySubmit: id => dispatch(tDeleteSubmit(id)),
  fetchUser: uiid => dispatch(fetchUser(uiid))
});


@connect(mapStateToProps, mapDispatchToProps)


class TemplateComponents extends React.Component {

  /**
   * [companyLogoFilePreview have to assign before state since our state depend on its ]
   * @type {String}
   */
  companyLogoFilePreview = "https://reviewkick.s3.amazonaws.com/uploads/ckeditor/pictures/15/content_amazon-logo.png"
  state = {
    tableData: [],
    data: [],
    pager: { ...defaultPagination },
    filterDropdownVisible: false,
    searchText: "",
    filtered: false,
    loading: true,
    createTemplateVisible: false,
    editorState: EditorState.createEmpty(),
    modifyTemplate: "",
    deteteTemplateId: "",
    showTemplatePreview: false,
    templateBody: "",
    templateTitle: "",
    templateOutput: "",
    orders: [],
    orderItem: {},
    selectedOrderId: "",
    redirectToTemplates: false,
    current: 0,

    template_name: "",
    template_subject: "",
    template_type: "General",
    template_status: "active",
    send_time: "12:00 am",
    send_day: "Immediately",
    send_after: "confirmed",
    minimum_item_condition: "All",
    fulfillment_type: "FBA and Merchant Fulfilled",

    template_name_error: false,
    template_subject_error: false,
    templateOutput_error: false,

    companyLogoFile: [],
    companyLogoFilePreview: this.companyLogoFilePreview,
    attachments: [],
    exclude_orders: [],
    attachments_preview: "",

  };

  componentWillMount() {
    // Fetch user data prior to component mounting
    const userId = cookie.load("uid");
    this.props.fetchUser(userId);
  }

  inputValidation = (key1, key2) => {
    let error = false;
    console.log("key1", key2);

    if (key1.length < 1) {
      this.setState({ [key2]: true });
      error = true;
    } else {
      this.setState({ [key2]: false });
      error = false;
    }
    return error;
  };


  next = () => {
    const {
      template_name,
      template_subject,
      templateOutput
    } = this.state;
    let error = false;
    error = this.inputValidation(template_name, "template_name_error");
    error = this.inputValidation(template_subject, "template_subject_error");
    error = this.inputValidation(templateOutput, "templateOutput_error");
    console.log("error", error);
    if (!error) {
      const current = this.state.current + 1;
      this.setState({ current });
    }else {
      // return;
    }
  };

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }


  changeStatus = (val) => {
    console.log(val);
  };

  onEditorStateChange = (editorState) => {
    this.setState({
      editorState
    });
    let messageBody = draftToHtml(convertToRaw(editorState.getCurrentContent()));
    let item = this.state.orderItem;
    if (!(typeof item === "object" && Object.keys(item).length)) {
      item = this.props.orders[0];
      this.setState({
        orderItem: item,
        selectedOrderId: item.AmazonOrderId
      });
    }
    let itemVar = {
      "buyer-name": item.BuyerName,
      "buyer-first-name": item.BuyerName ? item.BuyerName.split(" ").slice(0, -1).join(" ") : "",
      "thank-you-for-feedback": "Thank you text for leaving feedback. (Note - only shows if good feedback left)",
      "order-date": item.PurchaseDate,
      "order-id": item.AmazonOrderId,
      "product-name": item.orderItem[0].title,
      "product-price": item.OrderTotal.Amount,
      "product-qty": item.orderItem.reduce((a, c) => a + Number(c.qty), 0),
      "feedback-link": "https://www.amazon.com/gp/feedback/leave-customer-feedback.html/?order=SAMPLE-ORDER-ID&pageSize=1",
      "feedback-link-5star": `http://www.amazon.co.uk/gp/feedback/email-excellent-fb.html?ie=UTF8&excellent=2&isCBA=&marketplaceID=A1F83G8C2ARO7P&orderID=${item.AmazonOrderId}&rating=5&sellerID=YOUR-SELLER-ID`,
      "contact-link": `https://www.amazon.co.uk/ss/help/contact/?_encoding=UTF8&asin=&isCBA=&marketplaceID=A1F83G8C2ARO7P&orderID=&ref_=aag_d_sh&sellerID=YOUR-SELLER-ID`,
      "order-link": `https://www.amazon.com/gp/css/summary/edit.html?orderID=${item.AmazonOrderId}`,
      "review-link": `https://www.amazon.com/review/create-review?ie=UTF8&asin=B000000001&#`,
      "store-link": `https://www.amazon.co.uk/gp/aag/main/ref=olp_merch_name_4?ie=UTF8&asin=&isAmazonFulfilled=0&seller=YOUR-SELLER-ID`,
      "product-link": `https://www.amazon.co.uk/gp/product/B000000001`,
      "amazon-fba-contact-link": `https://www.amazon.co.uk/gp/help/customer/contact-us?ie=UTF8&orderId=${item.AmazonOrderId}&`
    };
    Mustache.parse(messageBody, ["[#", "#]"]);
    let final = Mustache.render(messageBody, itemVar);
    this.setState({
      templateOutput: final
    });
  };


  changeOrderTemplatePreview = (selValue) => {
    const self = this;
    const selectedOrder = this.props.orders.filter(or => or.AmazonOrderId === selValue);
    this.setState({
      orderItem: selectedOrder.length ? selectedOrder[0] : {},
      selectedOrderId: selValue
    });

    setTimeout(function() {
      self.onEditorStateChange(self.state.editorState);
    });
  };

  _handleOnChange = (key, value) => {
    this.setState({ [key]: value });
  };

  _submitTemplateModifyDetails = () => {
    const { templateModifySubmit } = this.props;
    const modifyTemplateId = this.props.match.params.id;
    const {
      template_name,
      template_subject,
      templateOutput,
      template_type,
      template_status,
      send_day,
      send_time,
      send_after,
      selectedOrderId,
      minimum_item_condition,
      fulfillment_type,
      attachments,
      companyLogoFile,
      exclude_orders,
    } = this.state;

    const formData = new FormData();
    attachments.forEach((file) => {
      formData.append("email_attachment", file);
    });
    companyLogoFile.forEach((file) => {
      formData.append("logo", file);
    });
    formData.append("exclude_orders", exclude_orders);

    formData.append("id", modifyTemplateId);

    formData.append("user_id", this.props.user._id);
    formData.append("template_name", template_name);
    formData.append("email_subject", template_subject);
    formData.append("template_type", template_type);
    formData.append("template_status", template_status);
    formData.append("email_message", templateOutput);
    formData.append("order_id", selectedOrderId);
    formData.append("send_day", send_day);
    formData.append("send_time", send_time);
    formData.append("send_after", send_after);
    formData.append("minimum_item_condition", minimum_item_condition);
    formData.append("fulfillment_type", fulfillment_type);

    // console log form data
    for (var pair of formData.entries()) {
      console.log('form_data ' + pair[0]+ ', ' + pair[1]);
    }
    // return;

    templateModifySubmit(formData);

    message.success("Processing complete!");
    this.setState({ redirectToTemplates: true });

  }
  transformExcludeOrders = (exclude_orders_obj) => {
    // let exclude_orders_obj =  { // it will come from api call
    //   "other": {
    //     "with_repeat_buyer": 1,
    //     "with_return": 1
    //   },
    //   "promotion": {
    //     "shipping_discount": 0,
    //     "item_discount": 0
    //   },
    //   "review": {
    //     "with_review_5": 0,
    //     "with_review_4": 0,
    //     "with_review_3": 0,
    //     "with_review_2": 1,
    //     "with_review_1": 1
    //   },
    //   "feedback": {
    //     "with_feedback_5": 0,
    //     "with_feedback_4": 0,
    //     "with_feedback_3": 0,
    //     "with_feedback_2": 1,
    //     "with_feedback_1": 1
    //   }
    // }
    let keyValues = {
        "with_repeat_buyer": 'other:repeat_buyer',
        "with_return": 'other:return',
        "shipping_discount": 'promotion:shipping_discount',
        "item_discount": 'promotion:item_discount',
        "with_review_5": 'review:5',
        "with_review_4": 'review:4',
        "with_review_3": 'review:3',
        "with_review_2": 'review:2',
        "with_review_1": 'review:1',
        "with_feedback_5": 'feedback:5',
        "with_feedback_4": 'feedback:4',
        "with_feedback_3": 'feedback:3',
        "with_feedback_2": 'feedback:2',
        "with_feedback_1": 'feedback:1',
      };
      let exclude_orders_obj_flat = {
        ...exclude_orders_obj.other,
        ...exclude_orders_obj.promotion,
        ...exclude_orders_obj.feedback,
        ...exclude_orders_obj.review,
      }

     let exclude_orders_arr = []
     Object.keys(exclude_orders_obj_flat).forEach(key => {
      if (exclude_orders_obj_flat[key] == 1) {
        exclude_orders_arr.push(keyValues[key])
      }
     })
     return exclude_orders_arr;
  }

  componentDidMount = () => {
    const modifyTemplateId = this.props.match.params.id;
    const self = this;
    const templates = this.props.templates;
    setTimeout(() => {
      if (self.props.templates && self.props.templates.length) {
        let templateData = self.props.templates.filter((temp) => temp["_id"] === modifyTemplateId)[0];
        const { form } = self.props;
        self.setState({ modifyTemplate: modifyTemplateId });
        if (typeof templateData === "object" && Object.keys(templateData).length) {
          console.log("templateData", templateData);
          const {
            template_name,
            order_id,
            email_subject,
            template_status,
            template_type,
            send_day,
            send_time,
            send_after,
            minimum_item_condition,
            fulfillment_type,
            email_message,
            logo,
            email_attachment,
            exclude_orders,

          } = templateData;

          const contentBlock = htmlToDraft(templateData.email_message);
          const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
          const editorState = EditorState.createWithContent(contentState);

          let logoPreview = this.companyLogoFilePreview;
          if (logo) {
            logoPreview = SERVER_STATIC_URL + logo
          }
          let attachments = []
          if (email_attachment) {
           let attachments_preview = SERVER_STATIC_URL + email_attachment
           attachments = [{
              uid: '1',
              name: this.imageName(email_attachment),
              status: 'done',
              url: attachments_preview,
            }]
          }

          let exclude_orders_arr = this.transformExcludeOrders(exclude_orders)

          self.setState({
            editorState,
            selectedOrderId: order_id,
            template_name,
            template_subject: email_subject,
            template_type,
            template_status,
            send_day,
            send_time,
            send_after,
            minimum_item_condition,
            fulfillment_type,
            templateOutput: email_message,
            companyLogoFilePreview: logoPreview,
            exclude_orders: exclude_orders_arr,
            attachments,

          });
        }
        self.setState({
          createTemplateVisible: true
        });
      }
    }, 1000);
  };

  imageName = (pathName) => {
    let arr = pathName.split('/');
    return arr[arr.length - 1]
  }

  _uploadFiles = (text, state_key, logoPreview = false) => {
    let defaultFileList = []
    let {attachments_preview} = this.state
    if (attachments_preview) {
     defaultFileList = [{
        uid: '1',
        name: this.imageName(attachments_preview),
        status: 'done',
        response: 'Server Error 500', // custom error message to show
        url: attachments_preview
      }]
    }

    let props = {
      onRemove: (file) => {
        this.setState((state) => {
          const uploadState = state[state_key]
          const index = uploadState.indexOf(file);
          const newUploads = uploadState.slice();
          newUploads.splice(index, 1);
          if (logoPreview) {
            return {
              [state_key]: newUploads,
              companyLogoFilePreview: this.companyLogoFilePreview // back to amazon logo
            }

          } else {
            return {
              [state_key]: newUploads
            };
          }
        });
      },
      beforeUpload: (file) => {
        if (logoPreview) {

          let supportedFormat = [
            'image/jpeg',
            'image/png',
            'image/gif',
          ]
          const isPic = supportedFormat.includes(file.type)
          if (!isPic) {
            message.error('You can only upload JPG file!');
          }else {
            let reader = new FileReader();
            reader.onloadend = () => {
              this.setState({
                companyLogoFile: [file],
                companyLogoFilePreview: reader.result
              });
            };
            reader.readAsDataURL(file);
          }

        }else {
          this.setState({[state_key]: [file] });
        }
        return false;
      },
      fileList: this.state[state_key],
    };
    return <div className="my-2">
      <Upload {...props}>
        <Button className='my-2'>
          <Icon type="upload"/> {text}
        </Button>
      </Upload>
    </div>;
  }

  handleUpload = () => {
    const { fileList } = this.state;
    const formData = new FormData();
    fileList.forEach((file) => {
      formData.append("files[]", file);
    });

    this.setState({
      uploading: true
    });

  };

  excludeOrderChange = (values, option) => {
      this.setState({ exclude_orders: values });
  }
  render() {
    console.log("this.state", this.state);
    // console.log('props of templates create', this.props);

    let {
      editorState,
      templateOutput,
      selectedOrderId,
      redirectToTemplates,
      current,
      companyLogoFilePreview,
      exclude_orders,
    } = this.state;
    let {
      template_name,
      template_subject,
      template_type,
      template_status,
      send_day,
      send_time,
      send_after,
      minimum_item_condition,
      fulfillment_type,

      template_name_error,
      template_subject_error,
      templateOutput_error

    } = this.state;

    let templates;
    let { getFieldDecorator } = this.props.form;
    const formHalfArea = {
      labelCol: { span: 4 },
      wrapperCol: { span: 14 }
    };

    let orders = [], orderOption = [];

    if (redirectToTemplates) {
      return <Redirect to='/templates'/>;
    }

    if (this.props.orders && this.props.orders.length) {
      orders = this.props.orders.map((or) => {
        return { value: or.AmazonOrderId, name: or.AmazonOrderId + ": " + or.orderItem[0].title };
      });
      orderOption = orders.map(ch => <Option key={ch.value}>{ch.name}</Option>);
    }

    const styleArr = [{ value: 1, name: "Default" }, { value: 2, name: "Blue" }, { value: 3, name: "Gray" }, {
      value: 4,
      name: "Green"
    }];
    const styleOption = styleArr.map(ch => <Option key={ch.value}>{ch.name}</Option>);

    const typeArr = [{ value: 1, name: "General" }, { value: 2, name: "Feedback Request" }, {
      value: 3,
      name: "Feedback Repair"
    },
      { value: 4, name: "Reviews" }, { value: 7, name: "Review Repair" }, {
        value: 5,
        name: "Shipping Info"
      }, { value: 6, name: "Product Info" }];
    const typeOption = typeArr.map(ch => <Option value={ch.name} key={ch.value}>{ch.name}</Option>);

    const tdStyle1 = { padding: "30px 0 0 0" };
    // display:block; height:77.3333px; margin:5px auto; vertical-align:top; width:160px
    const tdStyle2 = {
      display: "block",
      height: "77.3333px",
      margin: "5px auto",
      verticalAlign: "top",
      width: "160px"
    };
    const tdStyle3 = { padding: 5 };

    const sendDateArray = [{ name: "Immediately", value: 0 }, { name: "1 Day", value: 1 }, {
      name: "2 Days",
      value: 2
    }, { name: "3 Days", value: 3 },
      { name: "4 Days", value: 4 }, { name: "5 Days", value: 5 }, { name: "6 Days", value: 6 }, {
        name: "7 Days",
        value: 7
      }, { name: "8 Days", value: 8 },
      { name: "9 Days", value: 9 }, { name: "10 Days", value: 10 }, { name: "11 Days", value: 11 }, {
        name: "12 Days",
        value: 12
      }, { name: "13 Days", value: 13 },
      { name: "14 Days", value: 14 }, { name: "15 Days", value: 15 }, { name: "16 Days", value: 16 }, {
        name: "17 Days",
        value: 17
      }, { name: "18 Days", value: 18 },
      { name: "19 Days", value: 19 }, { name: "20 Days", value: 20 }, { name: "21 Days", value: 21 }, {
        name: "4 Weeks",
        value: 28
      }, { name: "5 Weeks", value: 35 },
      { name: "6 Weeks", value: 42 }, { name: "7 Weeks", value: 49 }, { name: "8 Weeks", value: 56 }, {
        name: "9 Weeks",
        value: 63
      }, { name: "10 Weeks", value: 70 }
    ];

    const sendDateOptions = sendDateArray.map(sd => <Option value={sd.name} key={sd.value}>{sd.name}</Option>);

    const whenOrderIsArray = [{ name: "Order Confirmed", value: "confirmed" }, {
      name: "Order Dispatched",
      value: "dispatched"
    },
      { name: "Order Delivered", value: "delivered" }, {
        name: "Positive Feedback Left",
        value: "positive_feedback_left"
      }];
    const whenOrderIsOptions = whenOrderIsArray.map(oi => <Option value={oi.value} key={oi.value}>{oi.name}</Option>);

    const minItemCondArr = ["All", "New", "Used Refurbished", "Collectible Acceptable", "Collectible Good", "Collectible Very Good",
      "Collectible Llike New", "Used Acceptable", "Used Good", "Used Very Good", "Used Like New"];
    const minItemCondOptions = minItemCondArr.map((ic, i) => <Option value={ic} key={i}>{ic}</Option>);

    const fulfilmentTypeArr = ["FBA and Merchant Fulfilled", "FBA only", "Merchant Fulfilled Only"];
    const fulfilmentTypeOptions = fulfilmentTypeArr.map((fft, i) => <Option value={fft} key={i}>{fft}</Option>);

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 4 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };

    let excludeOrderskeysOptions = pageData.excludeOrderKeys.map((option, i) =>
      <OptGroup key={i} label={option.title}>
        {
          option.keys.map((op, j) =>
            <Option key={op.value} value={op.value}>{op.label}</Option>
          )
        }
      </OptGroup>
    );


    let stepsContent;

    if (current === 0) {
      stepsContent = (
        <Row>
          <Col span={11}>
            <Form layout="horizontal" onSubmit={this._submitTemplateModifyDetails} className="create_template">
              <FormItem>
                <label className="form-label mb-0">Template Name:</label>
                <Input
                  onChange={e => this._handleOnChange("template_name", e.target.value)}
                  value={template_name} placeholder="Template name"/>
                {
                  template_name_error ?
                    <label className="form-label text-danger">Template Name can't be empty</label> : null
                }

              </FormItem>

              <FormItem>
                <label className="form-label mb-0">Email Subject:</label>

                <Input
                  value={template_subject}
                  onChange={e => this._handleOnChange("template_subject", e.target.value)}
                  placeholder="Email Subject"/>
                {
                  template_subject_error ?
                    <label className="form-label text-danger">Template email Subject can't be empty</label> : null
                }
              </FormItem>


              <FormItem>
                <Row>
                  <Col span={11}>
                    <FormItem>
                      <label className="form-label mb-0">Type:</label>
                      <Select value={template_type} onChange={value => this._handleOnChange("template_type", value)}>
                        {typeOption}
                      </Select>
                    </FormItem>
                  </Col>
                  <Col span={2}></Col>
                  <Col span={11}>
                    <FormItem>
                      <label className="form-label mb-0">Status:</label>
                      <Select value={template_status}
                              onChange={value => this._handleOnChange("template_status", value)}>
                        <Option value="active">Active</Option>
                        <Option value="inActive">Inactive</Option>
                      </Select>
                    </FormItem>
                  </Col>
                </Row>
              </FormItem>

              <br/>
              {this._uploadFiles('Select Attachments', 'attachments')}
              <br/>

              <FormItem>
                <label className="form-label mb-0">Message</label>
                <Editor
                  editorState={editorState}
                  wrapperClassName="demo-wrapper"
                  editorClassName="demo-editor"
                  onEditorStateChange={this.onEditorStateChange}
                  toolbarCustomButtons={[<CustomOption/>]}
                />
                {
                  templateOutput_error ?
                    <label className="form-label text-danger">Email body can't be empty</label> : null
                }

              </FormItem>

            </Form>
            {
              // <div className="btn btn-primary" onClick={this.submitCreateTemplate}>Create</div>
            }
          </Col>
          <Col span={10} offset={1}>
            <div className="template_preview">
              <div className="card">
                <div className="card-header">
                  <span>Live Email Preview</span> <span>View as: </span>Order #:<Select value={selectedOrderId}
                                                                                        style={{ width: "100%" }}
                                                                                        onChange={this.changeOrderTemplatePreview}>{orderOption}</Select>
                </div>
                <div className="card-body template_p_container">

                  <div className="m-2">
                    {this._uploadFiles('Upload Company logo', 'companyLogoFile', true)}
                  </div>
                  <table bgcolor="#f2f2f2" border="0" cellPadding="0" cellSpacing="0"
                         width="100%">
                    <tbody>
                    <tr>
                      <td align="center" style={tdStyle1}><img
                        className="fr-fic"
                        src={companyLogoFilePreview}
                        style={tdStyle2}/>
                      </td>
                    </tr>
                    <tr>
                      <td style={tdStyle3} className="template_area"
                          dangerouslySetInnerHTML={{ __html: templateOutput }}></td>
                    </tr>
                    <tr>
                      <td align="center">&nbsp;</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </Col>
        </Row>
      );
    } else {
      stepsContent = (
        <div className={"campaign_data"}>
          <Form layout="horizontal" className="create_campaign">


            <FormItem
              label="Choose when to send :"
              {...formItemLayout}>
              <Col span={6}>
                <FormItem>
                  &nbsp;
                  <Select onChange={value => this._handleOnChange("send_day", value)} value={send_day}
                          style={{ width: 120 }}>
                    {sendDateOptions}
                  </Select>
                </FormItem>
              </Col>
              <Col span={1}> &nbsp; at</Col>
              <Col span={6}>
                <FormItem>
                  <TimePicker
                    use12Hours
                    onChange={(time, timestring) => this._handleOnChange("send_time", timestring)}
                    defaultValue={moment(send_time, "h:mm a")}
                    defaultOpenValue={moment("00:00:00", "HH:mm a")}
                    format="h:mm a"
                    style={{ width: 110 }}
                  />
                </FormItem>
              </Col>
              <Col span={2}>after</Col>
              <Col span={9}>
                <FormItem>
                  <Select
                    value={send_after}
                    onChange={value => this._handleOnChange("send_after", value)}
                  >
                    {whenOrderIsOptions}
                  </Select>
                </FormItem>
              </Col>
            </FormItem>

            <FormItem>
              <Row>
                <Col span={4}>
                  <label className="form-label mb-0">Exclude Orders</label>
                </Col>
                <Col span={20}>
                  <Select
                      mode="multiple"
                      size="default"
                      value={exclude_orders}
                      placeholder="Please Select Exclusions"
                      onChange={this.excludeOrderChange}
                      style={{ width: '100%' }}>

                      {excludeOrderskeysOptions}
                  </Select>
                </Col>
              </Row>
            </FormItem>

            <FormItem>
              <label className="form-label mb-0">Minimum Item Condition:</label>
              <Select
                style={{ width: 180 }}
                value={minimum_item_condition}
                onChange={value => this._handleOnChange("minimum_item_condition", value)}
              >
                {minItemCondOptions}
              </Select>

            </FormItem>

            <FormItem>
              <label className="form-label mb-0">Fulfillment type:</label>
              <Select
                value={fulfillment_type}
                onChange={value => this._handleOnChange("fulfillment_type", value)}
                style={{ width: 180 }}>
                {fulfilmentTypeOptions}
              </Select>
            </FormItem>
          </Form>
        </div>
      );
    }

    return (


      <div className="card">
        <div className="card-body">
          <Steps current={current}>
            <Step key="Template" title="Template"/>
            <Step key="Campaign" title="Campaign"/>
          </Steps>
          <div className="steps-content">
            {stepsContent}
          </div>
          <div className="steps-action">
            {
              current < steps.length - 1
              && <Button type="primary" onClick={() => this.next()}>Next</Button>
            }
            {
              current === steps.length - 1
              &&
              <div className='d-inline'>
                <Button type="primary" onClick={() => this._submitTemplateModifyDetails()}>Done</Button>
              </div>
            }
            {
              current > 0
              && (
                <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
                  Previous
                </Button>
              )
            }
          </div>
        </div>
      </div>
    );
  }
}

// export default templates
const Template = Form.create()(TemplateComponents);
export default Template;
