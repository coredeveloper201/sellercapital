import React, { Component }  from 'react'
import { Redirect } from 'react-router-dom'
import {Table, Icon, Input, Button, DatePicker, Select, Radio, Form, Dropdown, Modal, Row, Col} from 'antd'
import { connect } from 'react-redux'
import { fetchTemplates, tCreationSubmit, tModifySubmit, tDeleteSubmit } from './../../../ducks/template'
import {fetchOrders} from './../../../ducks/order'
import './style.scss'

// Editor Draft js
import { EditorState, convertToRaw, ContentState, Modifier } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import PropTypes from 'prop-types';
import '../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import CustomOption from './CustomOption'

const Mustache = require('mustache');


const RangePicker = DatePicker.RangePicker;
const Option = Select.Option;
const FormItem = Form.Item;
const { TextArea } = Input;
const confirm = Modal.confirm;



const defaultPagination = {
    pageSizeOptions: ['20', '50', '100'],
    showSizeChanger: true,
    current: 1,
    size: 'small',
    showTotal: (total: number) => `Total ${total} items`,
    total: 0, pageSize: 50
}


const children = [];
let optionKeys = [{key: "AFN", value: "AFN"},{key: "MFN", value: "MFN"},{key: "Shipped", value: "Shipped"},
    {key: "Pending", value: "Pending"},{key: "Unshipped", value: "Unshipped"},
    {key: "Delivered", value: "Delivered"},{key: "Canceled", value: "Canceled"},{key: "Returned", value: "Returned"}];
for ( let i = 0; i < optionKeys.length; i++ ) {
    children.push(<Option key={optionKeys[i].key}>{optionKeys[i].value}</Option>);
}



const mapStateToProps = (state, props) => ({
    templates: state.template.templates,
    orders: state.order.orders
})


const mapDispatchToProps = (dispatch, props) => ({
    fetchTemplates: dispatch(fetchTemplates()),
    fetchOrders: dispatch(fetchOrders()),
    templateCreateSubmit: values => dispatch(tCreationSubmit(values)),
    templateModifySubmit: values => dispatch(tModifySubmit(values)),
    deleteModifySubmit: id => dispatch(tDeleteSubmit(id)),
})



@connect(mapStateToProps, mapDispatchToProps)


class TemplateComponents extends React.Component {

    constructor(props) {
        super(props);
    }

    state = {
        tableData: [],
        data: [],
        pager: { ...defaultPagination },
        filterDropdownVisible: false,
        searchText: '',
        filtered: false,
        loading: true,
        createTemplateVisible: false,
        editorState: EditorState.createEmpty(),
        modifyTemplate: '',
        deteteTemplateId: '',
        showTemplatePreview: false,
        templateBody: '',
        templateTitle: '',
        templateOutput: '',
        orders: [],
        orderItem: {},
        selectedOrderId: '',
        redirectToTemplates: false,
    }




    submitCreateTemplate = (e) => {
        e.preventDefault()
        const { form, dispatch } = this.props
        const {modifyTemplate} = this.state
        const self = this;
        this.props.form.validateFields((err, values) => {
            if (!err) {
                values.body = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
                if(!modifyTemplate){
                    this.props.templateCreateSubmit(values);
                }else{
                    values.id = modifyTemplate;
                    this.props.templateModifySubmit(values);
                }
            }else{
                console.log(err);
            }
            setTimeout(function () {
                self.setState({
                    redirectToTemplates: true
                })
            }, 500)
        })
    }

    componentDidMount = () => {
        console.log('all props inside componentDidMount', this.props);
        const modifyTemplateId = this.props.match.params.id;
        const self = this;
        const templates = this.props.templates;
        if( modifyTemplateId ){
            // const templateData = this.props.templates.filter((temp) => temp['_id'] === modifyTemplateId)[0];
            // this.setState({ modifyTemplate: modifyTemplateId });
            // console.log(modifyTemplateId);
            // console.log(templateData);
            // console.log(this.props.templates);
        }

        setTimeout(function () {
            if(self.props.templates && self.props.templates.length){
                let templateData = self.props.templates.filter((temp) => temp['_id'] === modifyTemplateId)[0];

                const { form } = self.props
                self.setState({ modifyTemplate: modifyTemplateId });
                if(typeof templateData === 'object' && Object.keys(templateData).length){
                    const {template_name, email_subject, template_status, template_type } = templateData;
                    form.setFieldsValue({template_name, email_subject, template_status, template_type });
                    const contentBlock = htmlToDraft(templateData.email_message);
                    const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
                    const editorState = EditorState.createWithContent(contentState);
                    self.setState({
                        editorState
                    });
                }
                self.setState({
                    createTemplateVisible: true,
                });
            }
        }, 1000);


    }

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        });
        let messageBody = draftToHtml(convertToRaw(editorState.getCurrentContent()));
        let item = this.state.orderItem;
        if(!(typeof item === 'object' && Object.keys(item).length )){
            item = this.props.orders[0];
            this.setState({
                orderItem: item,
                selectedOrderId: item.AmazonOrderId
            })
        }
        let itemVar = {'buyer-name': item.BuyerName, 'buyer-first-name': item.BuyerName ? item.BuyerName.split(' ').slice(0, -1).join(' '): '',
            'thank-you-for-feedback' : 'Thank you text for leaving feedback. (Note - only shows if good feedback left)',
            'order-date': item.PurchaseDate, 'order-id': item.AmazonOrderId, 'product-name': item.orderItem[0].title,
            'product-price': item.OrderTotal.Amount, 'product-qty': item.orderItem.reduce((a,c)=>a+Number(c.qty),0),
            'feedback-link': 'https://www.amazon.com/gp/feedback/leave-customer-feedback.html/?order=SAMPLE-ORDER-ID&pageSize=1',
            'feedback-link-5star': `http://www.amazon.co.uk/gp/feedback/email-excellent-fb.html?ie=UTF8&excellent=2&isCBA=&marketplaceID=A1F83G8C2ARO7P&orderID=${item.AmazonOrderId}&rating=5&sellerID=YOUR-SELLER-ID`,
            'contact-link': `https://www.amazon.co.uk/ss/help/contact/?_encoding=UTF8&asin=&isCBA=&marketplaceID=A1F83G8C2ARO7P&orderID=&ref_=aag_d_sh&sellerID=YOUR-SELLER-ID`,
            'order-link': `https://www.amazon.com/gp/css/summary/edit.html?orderID=${item.AmazonOrderId}`,
            'review-link': `https://www.amazon.com/review/create-review?ie=UTF8&asin=B000000001&#`,
            'store-link': `https://www.amazon.co.uk/gp/aag/main/ref=olp_merch_name_4?ie=UTF8&asin=&isAmazonFulfilled=0&seller=YOUR-SELLER-ID`,
            'product-link': `https://www.amazon.co.uk/gp/product/B000000001`,
            'amazon-fba-contact-link': `https://www.amazon.co.uk/gp/help/customer/contact-us?ie=UTF8&orderId=${item.AmazonOrderId}&`
        };
        Mustache.parse(messageBody, ['[#', '#]']);
        let final = Mustache.render(messageBody, itemVar);
        this.setState({
            templateOutput: final,
        });
    }


    changeOrderTemplatePreview = (selValue) => {
        const self = this;
        const selectedOrder = this.props.orders.filter( or => or.AmazonOrderId === selValue );
        this.setState({
            orderItem: selectedOrder.length ? selectedOrder[0] : {},
            selectedOrderId: selValue
        })

        setTimeout(function () {
            self.onEditorStateChange(self.state.editorState)
        })
    }


    render() {

        let {  editorState, templateOutput, selectedOrderId, redirectToTemplates } = this.state;
        let templates;
        const tdStyle1 = {padding: "30px 0 0 0"};
        // display:block; height:77.3333px; margin:5px auto; vertical-align:top; width:160px
        const tdStyle2 = {display: 'block', height: '77.3333px', margin: '5px auto', verticalAlign: 'top', width: '160px'};

        let { getFieldDecorator } = this.props.form
        const formHalfArea = {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 },
        }

        let orders = [], orderOption = [];

        if (redirectToTemplates) {
            return <Redirect to='/templates' />
        }

        if (this.props.orders && this.props.orders.length) {
            orders = this.props.orders.map((or) => { return {value: or.AmazonOrderId, name: or.AmazonOrderId + ': ' +or.orderItem[0].title} })
            orderOption = orders.map(ch => <Option key={ch.value}>{ch.name}</Option>);
        }

        const styleArr = [{value: 1, name: 'Default'}, {value: 2, name: 'Blue'}, {value: 3, name: 'Gray'}, {value: 4, name: 'Green'}];
        const styleOption = styleArr.map(ch => <Option key={ch.value}>{ch.name}</Option>);

        const statusArr = ["active", "inActive"]
        const statusOption = statusArr.map(ch => <Option value={ch} key={ch}>{ch}</Option> )

        const typeArr = [{value: 1, name: 'General'}, {value: 2, name: 'Feedback Request'}, {value: 3, name: 'Feedback Repair'},
            {value: 4, name: 'Reviews'}, {value: 7, name: 'Review Repair'}, {value: 5, name: 'Shipping Info'}, {value: 6, name: 'Product Info'}];
        const typeOption = typeArr.map(ch => <Option value={ch.name} key={ch.name}>{ch.name}</Option>);

        return (

                    <div className="card">
                        <div className="card-body">
                            <Row>
                                <Col span={11}>
                                    <Form layout="horizontal" onSubmit={this.submitCreateTemplate} className="create_template">
                                        <FormItem>
                                            <label className="form-label mb-0">Template Name:</label>
                                            {getFieldDecorator('template_name', {
                                                initialValue: '',
                                                rules: [{ required: false }],
                                            })(<Input placeholder="Template name" />)}
                                        </FormItem>

                                        <FormItem>
                                            <label className="form-label mb-0">Email Subject:</label>
                                            {getFieldDecorator('email_subject', {
                                                initialValue: '',
                                                rules: [{ required: true }],
                                            })(<Input placeholder="Email Subject" />)}
                                        </FormItem>


                                        <FormItem>
                                            <Row>
                                                <Col span={11}>
                                                    <FormItem>
                                                        <label className="form-label mb-0">Type:</label>
                                                        {getFieldDecorator('template_type', {
                                                            initialValue: 'Default',
                                                            rules: [{ required: false }],
                                                        })(<Select>
                                                            {typeOption}
                                                        </Select>)}
                                                    </FormItem>
                                                </Col>
                                                <Col span={2}></Col>
                                                <Col span={11}>
                                                    <FormItem>
                                                        <label className="form-label mb-0">Status</label>
                                                        {getFieldDecorator('template_status', {
                                                            initialValue: 'active',
                                                            rules: [{ required: false }],
                                                        })(<Select >
                                                            {statusOption}
                                                        </Select>)}
                                                    </FormItem>
                                                </Col>
                                            </Row>
                                        </FormItem>

                                        <br/>

                                        <FormItem>
                                            <label className="form-label mb-0">Message Body</label>

                                            {getFieldDecorator('email_message', {
                                                initialValue: '',
                                                rules: [{ required: false }],
                                            })(<Editor
                                                editorState={editorState}
                                                wrapperClassName="demo-wrapper"
                                                editorClassName="demo-editor"
                                                onEditorStateChange={this.onEditorStateChange}
                                                toolbarCustomButtons={[<CustomOption />]}
                                            />)}

                                        </FormItem>

                                    </Form>
                                    <div className="btn btn-primary" onClick={this.submitCreateTemplate}>Submit</div>
                                </Col>
                                <Col span={10} offset={1}>
                                    <div className="template_preview">
                                        <div className="card">
                                            <div className="card-header">
                                                <span>Live Email Preview</span> <span>View as: </span>Order #:<Select value={selectedOrderId} style={{ width: '100%' }} onChange={this.changeOrderTemplatePreview}>{orderOption}</Select>
                                            </div>
                                            {/*<div className="card-body template_p_container">*/}
                                                {/*<div className="template_area" dangerouslySetInnerHTML={{ __html: templateOutput }}></div>*/}
                                            {/*</div>*/}
                                            <div className="card-body template_p_container">

                                                <table bgcolor="#f2f2f2" border="0" cellPadding="0" cellSpacing="0"
                                                       width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" style={tdStyle1}><img
                                                            className="fr-fic"
                                                            src="https://reviewkick.s3.amazonaws.com/uploads/ckeditor/pictures/15/content_amazon-logo.png"
                                                            style={tdStyle2}/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style={{padding: 5}} className="template_area" dangerouslySetInnerHTML={{ __html: templateOutput }}></td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">&nbsp;</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>

                        </div>
                    </div>
        )
    }
}

// export default templates
const Template = Form.create()(TemplateComponents)
export default Template;
