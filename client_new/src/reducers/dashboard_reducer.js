import { FETCH_DASHBOARD, DASHBOARD_ERROR } from '../ducks/types';

const INITIAL_STATE = { dashboard: '', error: '' };

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_DASHBOARD:
            return { ...state, dashboard: action.payload.dashboard };
        case DASHBOARD_ERROR:
            return { ...state, error: action.payload };
        default:
    }

    return state;
}
