import { FETCH_TEMPLATES, DELETE_TEMPLATE, FETCH_SINGLE_TEMPLATE, TEMPLATE_ERROR } from '../ducks/types';

const INITIAL_STATE = { templates: [], template: '', error: '' };

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case FETCH_TEMPLATES:
            return { ...state, templates: action.payload.templates };
        case FETCH_SINGLE_TEMPLATE:
            return { ...state, template: action.payload.template };
        case DELETE_TEMPLATE:
            return { ...state, templates: action.payload.templates };
        case TEMPLATE_ERROR:
            return { ...state, error: action.payload };
        default:
    }

    return state;
}
